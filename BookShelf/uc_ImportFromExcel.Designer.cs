﻿namespace BookShelf
{
    partial class uc_ImportFromExcel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cb_Copys = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Discreption = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Sublect = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_BPrice = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Rating = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Collection = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Price = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Pages = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Date = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Publisher = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Author = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Group = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_Titel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cb_ISBN = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.spn_Start = new DevExpress.XtraEditors.SpinEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.cb_Sheets = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Copys.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Discreption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sublect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_BPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Rating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Collection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Price.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Pages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Publisher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Author.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Group.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Titel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_ISBN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Start.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sheets.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Location = new System.Drawing.Point(259, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(493, 525);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Preview";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 27);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(489, 496);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControl3.Controls.Add(this.labelControl10);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.labelControl16);
            this.groupControl3.Controls.Add(this.labelControl13);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.labelControl15);
            this.groupControl3.Controls.Add(this.labelControl12);
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Controls.Add(this.labelControl2);
            this.groupControl3.Controls.Add(this.labelControl11);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.labelControl3);
            this.groupControl3.Controls.Add(this.cb_Copys);
            this.groupControl3.Controls.Add(this.cb_Discreption);
            this.groupControl3.Controls.Add(this.cb_Sublect);
            this.groupControl3.Controls.Add(this.cb_BPrice);
            this.groupControl3.Controls.Add(this.cb_Rating);
            this.groupControl3.Controls.Add(this.cb_Collection);
            this.groupControl3.Controls.Add(this.cb_Price);
            this.groupControl3.Controls.Add(this.cb_Pages);
            this.groupControl3.Controls.Add(this.cb_Date);
            this.groupControl3.Controls.Add(this.cb_Publisher);
            this.groupControl3.Controls.Add(this.cb_Author);
            this.groupControl3.Controls.Add(this.cb_Group);
            this.groupControl3.Controls.Add(this.cb_Titel);
            this.groupControl3.Controls.Add(this.cb_ISBN);
            this.groupControl3.Controls.Add(this.simpleButton1);
            this.groupControl3.Controls.Add(this.spn_Start);
            this.groupControl3.Location = new System.Drawing.Point(0, 112);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(245, 416);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Columns";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(5, 392);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(68, 16);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "بدا من الصف";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(6, 357);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(34, 16);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "Copys";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 107);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(38, 16);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Author";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(6, 282);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(36, 16);
            this.labelControl16.TabIndex = 2;
            this.labelControl16.Text = "Rating";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(6, 232);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(28, 16);
            this.labelControl13.TabIndex = 2;
            this.labelControl13.Text = "Price";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(6, 157);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(69, 16);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "Publish date";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(6, 332);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(63, 16);
            this.labelControl14.TabIndex = 2;
            this.labelControl14.Text = "Discreption";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(6, 307);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(43, 16);
            this.labelControl15.TabIndex = 2;
            this.labelControl15.Text = "Subject";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(6, 257);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(73, 16);
            this.labelControl12.TabIndex = 2;
            this.labelControl12.Text = "Borrow price";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(6, 182);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(55, 16);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "Collection";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(6, 32);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(25, 16);
            this.labelControl9.TabIndex = 2;
            this.labelControl9.Text = "Titel";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 16);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "ISBN";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(6, 207);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(67, 16);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "PagesCount";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(6, 132);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(52, 16);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Publisher";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(6, 82);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(51, 16);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Category";
            // 
            // cb_Copys
            // 
            this.cb_Copys.Location = new System.Drawing.Point(90, 354);
            this.cb_Copys.Name = "cb_Copys";
            this.cb_Copys.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Copys.Size = new System.Drawing.Size(149, 22);
            this.cb_Copys.TabIndex = 1;
            // 
            // cb_Discreption
            // 
            this.cb_Discreption.Location = new System.Drawing.Point(90, 329);
            this.cb_Discreption.Name = "cb_Discreption";
            this.cb_Discreption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Discreption.Size = new System.Drawing.Size(149, 22);
            this.cb_Discreption.TabIndex = 1;
            // 
            // cb_Sublect
            // 
            this.cb_Sublect.Location = new System.Drawing.Point(90, 304);
            this.cb_Sublect.Name = "cb_Sublect";
            this.cb_Sublect.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Sublect.Size = new System.Drawing.Size(149, 22);
            this.cb_Sublect.TabIndex = 1;
            // 
            // cb_BPrice
            // 
            this.cb_BPrice.Location = new System.Drawing.Point(90, 254);
            this.cb_BPrice.Name = "cb_BPrice";
            this.cb_BPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_BPrice.Size = new System.Drawing.Size(149, 22);
            this.cb_BPrice.TabIndex = 1;
            // 
            // cb_Rating
            // 
            this.cb_Rating.Location = new System.Drawing.Point(90, 279);
            this.cb_Rating.Name = "cb_Rating";
            this.cb_Rating.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Rating.Size = new System.Drawing.Size(149, 22);
            this.cb_Rating.TabIndex = 1;
            // 
            // cb_Collection
            // 
            this.cb_Collection.Location = new System.Drawing.Point(90, 179);
            this.cb_Collection.Name = "cb_Collection";
            this.cb_Collection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Collection.Size = new System.Drawing.Size(149, 22);
            this.cb_Collection.TabIndex = 1;
            // 
            // cb_Price
            // 
            this.cb_Price.Location = new System.Drawing.Point(90, 229);
            this.cb_Price.Name = "cb_Price";
            this.cb_Price.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Price.Size = new System.Drawing.Size(149, 22);
            this.cb_Price.TabIndex = 1;
            // 
            // cb_Pages
            // 
            this.cb_Pages.Location = new System.Drawing.Point(90, 204);
            this.cb_Pages.Name = "cb_Pages";
            this.cb_Pages.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Pages.Size = new System.Drawing.Size(149, 22);
            this.cb_Pages.TabIndex = 1;
            // 
            // cb_Date
            // 
            this.cb_Date.Location = new System.Drawing.Point(90, 154);
            this.cb_Date.Name = "cb_Date";
            this.cb_Date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Date.Size = new System.Drawing.Size(149, 22);
            this.cb_Date.TabIndex = 1;
            // 
            // cb_Publisher
            // 
            this.cb_Publisher.Location = new System.Drawing.Point(90, 129);
            this.cb_Publisher.Name = "cb_Publisher";
            this.cb_Publisher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Publisher.Size = new System.Drawing.Size(149, 22);
            this.cb_Publisher.TabIndex = 1;
            // 
            // cb_Author
            // 
            this.cb_Author.Location = new System.Drawing.Point(90, 104);
            this.cb_Author.Name = "cb_Author";
            this.cb_Author.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Author.Size = new System.Drawing.Size(149, 22);
            this.cb_Author.TabIndex = 1;
            // 
            // cb_Group
            // 
            this.cb_Group.Location = new System.Drawing.Point(90, 79);
            this.cb_Group.Name = "cb_Group";
            this.cb_Group.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Group.Size = new System.Drawing.Size(149, 22);
            this.cb_Group.TabIndex = 1;
            // 
            // cb_Titel
            // 
            this.cb_Titel.Location = new System.Drawing.Point(90, 29);
            this.cb_Titel.Name = "cb_Titel";
            this.cb_Titel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Titel.Size = new System.Drawing.Size(149, 22);
            this.cb_Titel.TabIndex = 1;
            // 
            // cb_ISBN
            // 
            this.cb_ISBN.Location = new System.Drawing.Point(90, 54);
            this.cb_ISBN.Name = "cb_ISBN";
            this.cb_ISBN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_ISBN.Size = new System.Drawing.Size(149, 22);
            this.cb_ISBN.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(164, 389);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 22);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Add";
            // 
            // spn_Start
            // 
            this.spn_Start.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spn_Start.Location = new System.Drawing.Point(90, 389);
            this.spn_Start.Name = "spn_Start";
            this.spn_Start.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Start.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spn_Start.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spn_Start.Size = new System.Drawing.Size(72, 22);
            this.spn_Start.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.simpleButton2);
            this.groupControl2.Controls.Add(this.textEdit1);
            this.groupControl2.Controls.Add(this.cb_Sheets);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Location = new System.Drawing.Point(0, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(245, 107);
            this.groupControl2.TabIndex = 4;
            this.groupControl2.Text = "Excel file";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(160, 30);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(80, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Select";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(6, 31);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(148, 22);
            this.textEdit1.TabIndex = 0;
            // 
            // cb_Sheets
            // 
            this.cb_Sheets.Location = new System.Drawing.Point(5, 81);
            this.cb_Sheets.Name = "cb_Sheets";
            this.cb_Sheets.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Sheets.Size = new System.Drawing.Size(230, 22);
            this.cb_Sheets.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 59);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 16);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Sheets";
            // 
            // uc_ImportFromExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "uc_ImportFromExcel";
            this.Size = new System.Drawing.Size(777, 543);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Copys.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Discreption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sublect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_BPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Rating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Collection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Price.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Pages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Publisher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Author.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Group.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Titel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_ISBN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Start.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Sheets.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Copys;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Collection;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Date;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Publisher;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Author;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Group;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Titel;
        private DevExpress.XtraEditors.ComboBoxEdit cb_ISBN;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SpinEdit spn_Start;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Sheets;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit cb_BPrice;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Price;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Pages;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Discreption;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Sublect;
        private DevExpress.XtraEditors.ComboBoxEdit cb_Rating;
    }
}
