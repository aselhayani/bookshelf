﻿using System;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.DataAccess.Excel;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraSplashScreen;
using DevExpress.SpreadsheetSource;


namespace BookShelf
{
    public partial class uc_ImportFromExcel : DevExpress.XtraEditors.XtraUserControl
    {
      
        DataTable dataTable = new DataTable();
        public uc_ImportFromExcel(ref DataTable _dataTable)
        {
            InitializeComponent();
            dataTable = _dataTable;
            textEdit1.TextChanged += textEdit1_TextChanged;
            simpleButton1.Click += simpleButton1_Click;
            simpleButton2.Click += simpleButton2_Click;
            cb_Sheets.SelectedIndexChanged += cb_Sheets_SelectedIndexChanged;
            gridView1.DataSourceChanged += gridView1_DataSourceChanged;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            XtraOpenFileDialog dialog = new XtraOpenFileDialog();
            dialog.Filter = "Excel File(*.xls)|*.xls|Excel File(*.xlsx)|*.xlsx";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            textEdit1.Text = dialog.FileName;

        }
        private void textEdit1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                cb_Sheets.Properties.Items.Clear();
                cb_Sheets.Properties.Items.AddRange(GetExcelSheetNames(textEdit1.Text));

            }
            catch
            {

            }
        }
        public static string[] GetExcelSheetNames(string filelocation)
        {
            using (ISpreadsheetSource spreadsheetSource = SpreadsheetSourceFactory.CreateSource(filelocation))
            {
                IWorksheetCollection worksheetCollection = spreadsheetSource.Worksheets;
                return worksheetCollection.Select(x => x.Name).ToArray();
            }
        }
        private void cb_Sheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_Sheets.SelectedIndex >= 0 && cb_Sheets.Text != string.Empty)
            {
                try
                {
                    gridControl1.DataSource = null;
                    gridView1.Columns.Clear();

                    ExcelDataSource ds = new ExcelDataSource();
                    ds.FileName = textEdit1.Text;
                    DevExpress.DataAccess.Excel.ExcelSourceOptions excelSourceOptions1 = new DevExpress.DataAccess.Excel.ExcelSourceOptions();
                    DevExpress.DataAccess.Excel.ExcelWorksheetSettings excelWorksheetSettings1 = new DevExpress.DataAccess.Excel.ExcelWorksheetSettings();
                    excelWorksheetSettings1.WorksheetName = cb_Sheets.Text;
                    excelSourceOptions1.ImportSettings = excelWorksheetSettings1;
                    ds.SourceOptions = excelSourceOptions1;
                    ds.Fill();

                    gridControl1.DataSource = ds;
                    gridView1.PopulateColumns();

                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }
        private void gridView1_DataSourceChanged(object sender, EventArgs e)
        {
            cb_Author.Properties.Items.Clear();
            cb_BPrice.Properties.Items.Clear();
            cb_Collection.Properties.Items.Clear();
            cb_Copys.Properties.Items.Clear();
            cb_Group.Properties.Items.Clear();
            cb_Date.Properties.Items.Clear();
            cb_Discreption.Properties.Items.Clear();
            cb_ISBN.Properties.Items.Clear();
            cb_Pages.Properties.Items.Clear();
            cb_Price.Properties.Items.Clear();
            cb_Publisher.Properties.Items.Clear();
            cb_Rating.Properties.Items.Clear();
            cb_Titel.Properties.Items.Clear();
            cb_Sublect.Properties.Items.Clear();

            cb_Author.Properties.Items.Add("");
            cb_BPrice.Properties.Items.Add("");
            cb_Collection.Properties.Items.Add("");
            cb_Copys.Properties.Items.Add("");
            cb_Group.Properties.Items.Add("");
            cb_Date.Properties.Items.Add("");
            cb_Discreption.Properties.Items.Add("");
            cb_ISBN.Properties.Items.Add("");
            cb_Pages.Properties.Items.Add("");
            cb_Price.Properties.Items.Add("");
            cb_Publisher.Properties.Items.Add("");
            cb_Rating.Properties.Items.Add("");
            cb_Titel.Properties.Items.Clear();
            cb_Sublect.Properties.Items.Add("");
            var columns = gridView1.Columns.Select(x => x.FieldName).ToList();
            cb_Author.Properties.Items.AddRange(columns);
            cb_BPrice.Properties.Items.AddRange(columns);
            cb_Collection.Properties.Items.AddRange(columns);
            cb_Copys.Properties.Items.AddRange(columns);
            cb_Group.Properties.Items.AddRange(columns);
            cb_Date.Properties.Items.AddRange(columns);
            cb_Discreption.Properties.Items.AddRange(columns);
            cb_ISBN.Properties.Items.AddRange(columns);
            cb_Pages.Properties.Items.AddRange(columns);
            cb_Price.Properties.Items.AddRange(columns);
            cb_Publisher.Properties.Items.AddRange(columns);
            cb_Rating.Properties.Items.AddRange(columns);
            cb_Titel.Properties.Items.AddRange(columns);
            cb_Sublect.Properties.Items.AddRange(columns);

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {


            IOverlaySplashScreenHandle overlayHandle = SplashScreenManager.ShowOverlayForm(this, false, false, Color.Gray, Color.Navy, 45);


            for (int i = Convert.ToInt32(spn_Start.EditValue) - 1; i < gridView1.RowCount; i++)
            {
                DataRow row = dataTable.NewRow();
                string ISBN = gridView1.GetRowCellValue(i, cb_ISBN.Text) as string;
                string Titel = gridView1.GetRowCellValue(i, cb_Titel.Text) as string;
                string Publisher = gridView1.GetRowCellValue(i, cb_Publisher.Text) as string;
                string Collection = gridView1.GetRowCellValue(i, cb_Collection.Text) as string;
                string Subject = gridView1.GetRowCellValue(i, cb_Sublect.Text) as string;
                string Discreption = gridView1.GetRowCellValue(i, cb_Discreption.Text) as string;
                string Category = gridView1.GetRowCellValue(i, cb_Group.Text) as string;
                string Author = gridView1.GetRowCellValue(i, cb_Author.Text) as string;
                int? PagesCount = gridView1.GetRowCellValue(i, cb_Pages.Text) as int?;
                double? Price = gridView1.GetRowCellValue(i, cb_Price.Text) as double?;
                double? BorrowPrice = gridView1.GetRowCellValue(i, cb_BPrice.Text) as double?;
                byte? Rate = gridView1.GetRowCellValue(i, cb_Rating.Text) as byte?;
                DateTime date = DateTime.Now;
                if (gridView1.GetRowCellValue(i, cb_Date.Text) != null)
                {
                    if (!DateTime.TryParse(gridView1.GetRowCellValue(i, cb_Date.Text).ToString(), out date))
                        DateTime.TryParse((gridView1.GetRowCellValue(i, cb_Date.Text).ToString() + "-1-1"), out date);
                }
                row["ISBN"] = ISBN;
                row["Titel"] = Titel;
                row["Publisher"] = Publisher;
                row["Collection"] = Collection;
                row["Subject"] = Subject;
                row["Discreption"] = Discreption;
                row["Category"] = Category;
                row["Author"] = Author;
                row["PagesCount"] = PagesCount;
                row["Price"] = Price;
                row["BorrowPrice"] = BorrowPrice;
                row["Rating"] = Rate;
                row["PublishDate"] = date;
                dataTable.Rows.Add(row);

            }
            SplashScreenManager.CloseOverlayForm(overlayHandle);
            this.Dispose();
        }


    }

}

