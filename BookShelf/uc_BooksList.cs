﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BookShelf
{
    public partial class uc_BooksList : DevExpress.XtraEditors.XtraUserControl
    {
        private static uc_BooksList _instance;
        public static uc_BooksList instance
        {
            get
            {
                if (_instance == null)
                    _instance = new uc_BooksList();
                return _instance;
            }
        }
        public uc_BooksList()
        {
            InitializeComponent();
        }

        public void RefreshData()
        {
            gridControl1.DataSource = (new BookShelfDBDataContext().Books.ToList());
        }
        private void uc_BooksList_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            BookShelfDBDataContext db = new BookShelfDBDataContext();
            uc_Book.instance.book = db.Books.Single(x => x.ID == Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID")));
             
            frm_Main.instance . ShowControl(uc_Book.instance);

        }
    }
}
