﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BookShelf
{
    public partial class uc_AddListOfBooks : DevExpress.XtraEditors.XtraUserControl
    {

        private static uc_AddListOfBooks _instance;

        public static uc_AddListOfBooks instance
        {
            get
            {
                if (_instance == null)
                    _instance = new uc_AddListOfBooks();
                return _instance;
            }
        }

        DataTable table = new DataTable();

        public uc_AddListOfBooks()
        {
            InitializeComponent();
            table.Columns.Add("ISBN");
            table.Columns.Add("Titel");
            table.Columns.Add("Publisher");
            table.Columns.Add("PublishDate");
            table.Columns.Add("Collection");
            table.Columns.Add("Author");
            table.Columns.Add("Category");
            table.Columns.Add("PagesCount");
            table.Columns.Add("Price");
            table.Columns.Add("BorrowPrice");
            table.Columns.Add("Country");
            table.Columns.Add("Subject");
            table.Columns.Add("Discreption");
            table.Columns.Add("Rating");
            table.Columns.Add("Copys");
            memoEdit1.EditValueChanged += memoEdit1_EditValueChanged;
            memoEdit1.ReadOnly = true;  
            gridControl1.DataSource = table;

        }
        void memoEdit1_EditValueChanged(object sender, EventArgs e)
        {
            BeginInvoke(new MethodInvoker(delegate
            {
                SetSelection();
            }));
        }
        private void SetSelection()
        {
            memoEdit1.SelectionStart = memoEdit1.Text.Length;
            memoEdit1.ScrollToCaret();
        }

        void New()
        {
            table.Clear();
            layoutControl1.Enabled = true; 
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            New();
        }
       
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Task.Run(() => Save());

        }
        void Save()
        {
            Invoke((MethodInvoker)(() => progressBarControl1.Properties.Maximum = table.Rows.Count));
            List<Book> books = new List<Book>(); 
            using (BookShelfDBDataContext db = new BookShelfDBDataContext())
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    string ISBN = table.Rows[i]["ISBN"] as string;
                    string Titel = table.Rows[i]["Titel"] as string;
                    string Publisher = table.Rows[i]["Publisher"] as string;
                    string Collection = table.Rows[i]["Collection"] as string;
                    string Country = table.Rows[i]["Country"] as string;
                    string Subject = table.Rows[i]["Subject"] as string;
                    string Discreption = table.Rows[i]["Discreption"] as string;
                    int? PagesCount = table.Rows[i]["PagesCount"] as int?;
                    double? Price = table.Rows[i]["Price"] as double?;
                    double? BorrowPrice = table.Rows[i]["BorrowPrice"] as double?;
                    byte? Rate = table.Rows[i]["Rating"] as byte ?;
                    DateTime dateTime = DateTime.Now;


                    if (table.Rows[i]["PublishDate"] != null)
                        DateTime.TryParse(table.Rows[i]["PublishDate"].ToString(), out dateTime);
                    if (dateTime.Year.ToString() == "1" || dateTime.Year > 2070)
                        dateTime = DateTime.Now;


                    Book book = new Book()
                    {
                        ISBN = ISBN??"",
                        Titel = Titel??"",
                        Publisher = Publisher??"",
                        PublishDate = dateTime,
                        Collection = Collection??"",
                        PagesCount = PagesCount??0,
                        Price = Price ?? 0,
                        BorrowPrice = BorrowPrice??0,
                        Country = Country??"",
                        Subject = Subject ?? "",
                        Discreption = Discreption??"",
                        Rate  = Rate??0,
                        
                    };
                    db.Books.InsertOnSubmit(book);
                    db.SubmitChanges();

                    var author = db.Authors.FirstOrDefault(x => x.Name == table.Rows[i]["Author"].ToString());
                    if (author == null)
                    {
                        author = new Author()
                        {
                            Name = table.Rows[i]["Author"].ToString()
                        };
                        db.Authors.InsertOnSubmit(author);
                        db.SubmitChanges();
                    }
                    db.BookAuthors.InsertOnSubmit(new BookAuthor() { BookID = book.ID, AuthorID = author.ID });
                    var category = db.Categories.FirstOrDefault(x => x.Name == table.Rows[i]["Category"].ToString());
                    if (category == null)
                    {
                        category = new Category()
                        {
                            Name = table.Rows[i]["Category"].ToString()
                        };
                        db.Categories.InsertOnSubmit(category);
                        db.SubmitChanges();
                    }
                    db.BookCategories.InsertOnSubmit(new BookCategory() { BookID = book.ID, CategoryID = category.ID });
                    db.SubmitChanges();
                    books.Add(book);

                    Invoke((MethodInvoker)(()=> memoEdit1.Text += "'"+book.Titel +"' add successfuly"+ Environment.NewLine ));
                    Invoke((MethodInvoker)(() => progressBarControl1.Position = i));
                }

            };

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            frm_Main.instance.ShowControl(new uc_ImportFromExcel(ref this.table));
        }
    }
}
