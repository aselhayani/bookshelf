﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BookShelf
{
    public partial class uc_Member : DevExpress.XtraEditors.XtraUserControl
    {
        private static  uc_Member _instance;

        public static uc_Member instance
        {
            get
            {
                if (_instance == null)
                    _instance = new uc_Member();
                return _instance;
            }
        }
        public uc_Member()
        {
            InitializeComponent();
        }
    }
}
