﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookShelf
{
    public partial class uc_Book : DevExpress.XtraEditors.XtraUserControl
    {
      
        private  Book _book;
        
        public Book book
        {
            get { return _book; }
            set
            {
                _book = value;
                GetData();
            }
        }
        
        List<BookAuthor> bookAuthors;
        List<BookCategory> bookCategory;

        private static uc_Book _instance;

        public static uc_Book instance {
            get
            {
                if (_instance == null)
                    _instance = new uc_Book();
                return _instance;
            }
        }

        public uc_Book()
        {
            InitializeComponent();
            RefreshData();
            GetData();
        }
        bool ValidateDate()
        {
            return true;
        }
        void RefreshData()
        {
            BookShelfDBDataContext db = new BookShelfDBDataContext();
            var colection = db.Books.Select(x => new { Name = x.Collection }).Distinct().ToList();
            var Langage = db.Books.Select(x => new { Name = x.Language }).Distinct().ToList();
            var Countrys = db.Books.Select(x => new { Name = x.Country }).Distinct().ToList();
            txt_Publisher.Properties.DataSource = db.Books.Select(x => new { Name = x.Publisher }).Distinct().ToList();
            txt_Collection.Properties.DataSource = colection;

            txt_Country.Properties.DataSource = Countrys;
            txt_Langage.Properties.DataSource = Langage;
            txt_Collection.Properties.DisplayMember = txt_Collection.Properties.ValueMember =
            txt_Langage.Properties.DisplayMember = txt_Langage.Properties.ValueMember =
            txt_Publisher.Properties.DisplayMember = txt_Publisher.Properties.ValueMember =
            txt_Country.Properties.DisplayMember = txt_Country.Properties.ValueMember = "Name";

        }
        void Save()
        {
            if (!ValidateDate()) return;
            BookShelfDBDataContext db = new BookShelfDBDataContext();
            _book = db.Books.Where(x => x.ID.ToString() == txt_ID.Text).SingleOrDefault();
            if (_book == null)
            {
                _book = new Book();
                db.Books.InsertOnSubmit(_book);
            }
            SetData();
            db.SubmitChanges();
            db.BookAuthors.DeleteAllOnSubmit(db.BookAuthors.Where(x => x.BookID == _book.ID));
            db.BookCategories.DeleteAllOnSubmit(db.BookCategories.Where(x => x.BookID == _book.ID));
            db.SubmitChanges();
            foreach (var item in bookAuthors)
                item.BookID = _book.ID;
            foreach (var item in bookCategory)
                item.BookID = _book.ID;
            db.BookAuthors.InsertAllOnSubmit(bookAuthors);
            db.BookCategories.InsertAllOnSubmit(bookCategory);
            db.SubmitChanges();
            frm_Main.ShowMessage("Book add successfully");
            uc_BooksList.instance.RefreshData();
            RefreshData();
            GetData();
        }

        void SetData()
        {
            if (_book == null)
                _book = new Book();
            _book.Collection = txt_Collection.Text;
            _book.Discreption = txt_Discreption.Text;
            _book.ISBN = txt_ISBN.Text;
            _book.PublishDate = txt_PublishDate.DateTime;
            _book.Publisher = txt_Publisher.Text;
            _book.Titel = txt_Titel.Text;

            _book.Language = txt_Langage.Text;
            _book.PagesCount = Convert.ToInt32(spn_PagesCount.EditValue);
            _book.Price = Convert.ToDouble(spn_Price.EditValue);
            _book.BorrowPrice = Convert.ToDouble(spn_BPrice.EditValue);
            _book.Cover = frm_Main.GetByteFromImage(pictureEdit1.Image);
            _book.Rate = Convert.ToByte(ratingControl1.Rating);
            _book.Subject = txt_Subject.Text;
            _book.Country = txt_Country.Text;

        }
        void GetData(bool DontGetAutorsAndCatelogs = false )
        {
            if (_book == null)
                _book = new Book();
            txt_Collection.Text = _book.Collection;
            txt_Discreption.Text = _book.Discreption;
            txt_ID.Text = _book.ID.ToString();
            txt_ISBN.Text = _book.ISBN;
            txt_PublishDate.EditValue  = _book.PublishDate;
            txt_Publisher.Text = _book.Publisher;
            txt_Titel.Text = _book.Titel;
            txt_Langage.Text = _book.Language;
            spn_PagesCount.EditValue = _book.PagesCount;
            spn_Price.EditValue = _book.Price;
            spn_BPrice.EditValue = _book.BorrowPrice;
            ratingControl1.Rating = _book.Rate??0;
            txt_Subject.Text = _book.Subject;
            txt_Country.Text=  _book.Country;

            try { pictureEdit1.Image = frm_Main.GetImageFromByte(_book.Cover.ToArray()); } catch { }
            if (!DontGetAutorsAndCatelogs)
            {
                BookShelfDBDataContext db = new BookShelfDBDataContext();
                bookAuthors = db.BookAuthors.Where(x => x.BookID == _book.ID).ToList();
                bookCategory = db.BookCategories.Where(x => x.BookID == _book.ID).ToList();
            }
         
            if (bookAuthors == null)
                bookAuthors = new List<BookAuthor>();
            if (bookCategory == null)
                bookCategory = new List<BookCategory>();
            ViewauthorsNames();
            ViewCategoryNames();

        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
        }

        private void textEdit13_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            bookAuthors = uc_BookAuthors.ViewAuthors(new BindingList<BookAuthor>(bookAuthors)).ToList();
            ViewauthorsNames();


        }
        void ViewauthorsNames()
        {
            BookShelfDBDataContext db = new BookShelfDBDataContext();
            var authorsNames = db.Authors.Where(x => bookAuthors.Select(y => y.AuthorID).Contains(x.ID)).Select(x => x.Name).ToList();
            textEdit13.Text = "";
            foreach (var item in authorsNames)
            {
                textEdit13.Text += " - " + item;
            }
            textEdit13.Text += "   ";
            textEdit13.Text = textEdit13.Text.Remove(0, 2).Trim();

        }
        void ViewCategoryNames()
        {
            BookShelfDBDataContext db = new BookShelfDBDataContext();
            var CategoryNames = db.Categories .Where(x => bookCategory.Select(y => y.CategoryID  ).Contains(x.ID)).Select(x => x.Name).ToList();
            textEdit14.Text = ""; 
            foreach (var item in CategoryNames)
            {
                textEdit14.Text += " - " + item;
            }
            textEdit14.Text += "   "; 
            textEdit14.Text = textEdit14.Text.Remove(0, 2).Trim();
        }
        private void textEdit14_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            bookCategory  = uc_BookAuthors.ViewCategory (new BindingList<BookCategory>(bookCategory)).ToList();
            ViewCategoryNames();
        }

        public static byte  DownloadBookData(Book book, out  List<BookAuthor> _bookAuthors , out  List<BookCategory> _bookCategorys )
        { 
            string results;
            _bookAuthors = new List<BookAuthor>();
            _bookCategorys = new List<BookCategory>();
            try
            {
                var url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + book.ISBN + "&key" + Properties.Settings.Default.GoogleBookAPiKey;
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    results = wc.DownloadString(url); 
                }
            }
            catch
            { 
                return 0;
            } 

            Rootobject rob = Newtonsoft.Json.JsonConvert.DeserializeObject<Rootobject>(results);

            if (rob.totalItems == 0)
            {
                try
                {
                    var url = "https://www.googleapis.com/books/v1/volumes?q=title:" + book.Titel+ "&key="+Properties.Settings.Default.GoogleBookAPiKey ;
                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = Encoding.UTF8;
                        results = wc.DownloadString(url);
                    }
                }
                catch
                {
                    return 0;
                }
                rob = Newtonsoft.Json.JsonConvert.DeserializeObject<Rootobject>(results);
            }

            BookShelfDBDataContext db = new BookShelfDBDataContext();

            if (rob.totalItems > 0)
            { 
                if (rob.items.Count() > 0)
                { 
                    if (rob.items[0].saleInfo != null && rob.items[0].saleInfo.retailPrice != null)
                    {
                        book.Price = rob.items[0].saleInfo.retailPrice.amount;
                    }
                    if (rob.items[0].volumeInfo.authors != null && rob.items[0].volumeInfo.authors.Count() > 0)
                    {
                        _bookAuthors = new List<BookAuthor>();
                        foreach (var item in rob.items[0].volumeInfo.authors)
                        { 
                            var author = db.Authors.FirstOrDefault(x => x.Name == item);
                            if (author == null)
                            {
                                author = new Author()
                                {
                                    Name = item
                                };
                                db.Authors.InsertOnSubmit(author);
                                db.SubmitChanges();
                            }
                            _bookAuthors.Add(new BookAuthor() { BookID = book.ID, AuthorID = author.ID });
                        } 
                    }
                    if (rob.items[0].volumeInfo.categories != null && rob.items[0].volumeInfo.categories.Count() > 0)
                    {
                        _bookCategorys = new List<BookCategory>();
                        foreach (var item in rob.items[0].volumeInfo.categories)
                        { 
                            var category = db.Categories.FirstOrDefault(x => x.Name == item);
                            if (category == null)
                            {
                                category = new Category()
                                {
                                    Name = item
                                };
                                db.Categories.InsertOnSubmit(category);
                                db.SubmitChanges();
                            }
                            _bookCategorys.Add(new BookCategory() { BookID = book.ID, CategoryID = category.ID });
                        } 
                    }
                    if (rob.items[0].volumeInfo != null)
                    {
                        book.Titel = rob.items[0].volumeInfo.title;
                        book.Discreption = rob.items[0].volumeInfo.description;
                        book.Publisher = rob.items[0].volumeInfo.publisher;
                        if (rob.items[0].volumeInfo.industryIdentifiers != null && rob.items[0].volumeInfo.industryIdentifiers.Count() > 1) 
                            book.ISBN = rob.items[0].volumeInfo.industryIdentifiers[1].identifier;
                         else if(rob.items[0].volumeInfo.industryIdentifiers != null)
                            book.ISBN = rob.items[0].volumeInfo.industryIdentifiers[0].identifier;

                        try
                        {
                            string dt = rob.items[0].volumeInfo.publishedDate; 
                            DateTime date = new DateTime(); 
                            if (!DateTime.TryParse(dt, out date))
                                DateTime.TryParse((dt + "-1-1"), out date);
                            book.PublishDate = date;

                        }    catch { }
                        try
                        {
                            WebRequest req = WebRequest.Create(rob.items[0].volumeInfo.imageLinks.smallThumbnail);
                            WebResponse response = req.GetResponse();
                            MemoryStream strm = new MemoryStream();
                            Stream stream = response.GetResponseStream();
                            stream.CopyTo(strm);
                            book.Cover = strm.ToArray();
                            
                        }     catch    {   } 
                        book.PagesCount = rob.items[0].volumeInfo.pageCount;
                        book.Language = rob.items[0].volumeInfo.language;
                        book.Rate = Convert.ToByte(rob.items[0].volumeInfo.averageRating);
                    } 
                } 
            }
            else
              return 1;

            return 2;

        }

        private void txt_ISBN_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            book.ISBN = txt_ISBN.Text.Trim();
            DownloadBookData(book,out  bookAuthors ,out  bookCategory );
            GetData(true);
        }

        private void txt_Publisher_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            if (e.DisplayValue != null || e.DisplayValue.ToString().Trim() != "")
            {
                BookShelfDBDataContext db = new BookShelfDBDataContext();
                 
                e.Handled = true;
            }
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _book = new Book();
            GetData();
            pictureEdit1.Image = null; 
        }
    }



    #region GoogleBookInfo


    public class Listprice
    {
        public float amount { get; set; }
        public string currencyCode { get; set; }
    }

    public class Retailprice
    {
        public float amount { get; set; }
        public string currencyCode { get; set; }
    }

    public class Offer
    {
        public int finskyOfferType { get; set; }
        public Listprice1 listPrice { get; set; }
        public Retailprice1 retailPrice { get; set; }
    }

    public class Listprice1
    {
        public float amountInMicros { get; set; }
        public string currencyCode { get; set; }
    }

    public class Retailprice1
    {
        public float amountInMicros { get; set; }
        public string currencyCode { get; set; }
    }



    public class Rootobject
    {
        public string kind { get; set; }
        public int totalItems { get; set; }
        public Item[] items { get; set; }
    }

    public class Item
    {
        public string kind { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public string selfLink { get; set; }
        public Volumeinfo volumeInfo { get; set; }
        public Saleinfo saleInfo { get; set; }
        public Accessinfo accessInfo { get; set; }
        public Searchinfo searchInfo { get; set; }
    }

    public class Volumeinfo
    {
        public string title { get; set; }
        public string[] authors { get; set; }
        public string publisher { get; set; }
        public string publishedDate { get; set; }
        public string description { get; set; }
        public Industryidentifier[] industryIdentifiers { get; set; }
        public Readingmodes readingModes { get; set; }
        public int pageCount { get; set; }
        public string printType { get; set; }
        public string[] categories { get; set; }
        public float averageRating { get; set; }
        public int ratingsCount { get; set; }
        public string maturityRating { get; set; }
        public bool allowAnonLogging { get; set; }
        public string contentVersion { get; set; }
        public Imagelinks imageLinks { get; set; }
        public string language { get; set; }
        public string previewLink { get; set; }
        public string infoLink { get; set; }
        public string canonicalVolumeLink { get; set; }


    }

    public class Readingmodes
    {
        public bool text { get; set; }
        public bool image { get; set; }
    }

    public class Imagelinks
    {
        public string smallThumbnail { get; set; }
        public string thumbnail { get; set; }
    }

    public class Industryidentifier
    {
        public string type { get; set; }
        public string identifier { get; set; }
    }

    public class Saleinfo
    {
        public string country { get; set; }
        public string saleability { get; set; }
        public bool isEbook { get; set; }
        public Listprice listPrice { get; set; }
        public Retailprice retailPrice { get; set; }
        public string buyLink { get; set; }
        public Offer[] offers { get; set; }

    }

    public class Accessinfo
    {
        public string country { get; set; }
        public string viewability { get; set; }
        public bool embeddable { get; set; }
        public bool publicDomain { get; set; }
        public string textToSpeechPermission { get; set; }
        public Epub epub { get; set; }
        public Pdf pdf { get; set; }
        public string webReaderLink { get; set; }
        public string accessViewStatus { get; set; }
        public bool quoteSharingAllowed { get; set; }
    }

    public class Epub
    {
        public bool isAvailable { get; set; }
    }

    public class Pdf
    {
        public bool isAvailable { get; set; }
    }

    public class Searchinfo
    {
        public string textSnippet { get; set; }
    }






    #endregion

}
