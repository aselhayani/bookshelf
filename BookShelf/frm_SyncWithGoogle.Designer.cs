﻿namespace BookShelf
{
    partial class frm_SyncWithGoogle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.chk_Title = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Publisher = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Authors = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Categorys = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Cover = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Price = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.chk_Date = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Pages = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Rating = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Subjrct = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Discreption = new DevExpress.XtraEditors.CheckEdit();
            this.chk_Isbn = new DevExpress.XtraEditors.CheckEdit();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Title.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Publisher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Authors.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Categorys.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Cover.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Price.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Pages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Rating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Subjrct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Discreption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Isbn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(11, 288);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Properties.WordWrap = false;
            this.memoEdit1.Size = new System.Drawing.Size(317, 176);
            this.memoEdit1.TabIndex = 0;
            // 
            // chk_Title
            // 
            this.chk_Title.EditValue = true;
            this.chk_Title.Location = new System.Drawing.Point(5, 31);
            this.chk_Title.Name = "chk_Title";
            this.chk_Title.Properties.Caption = "Title";
            this.chk_Title.Size = new System.Drawing.Size(103, 20);
            this.chk_Title.TabIndex = 2;
            // 
            // chk_Publisher
            // 
            this.chk_Publisher.EditValue = true;
            this.chk_Publisher.Location = new System.Drawing.Point(5, 57);
            this.chk_Publisher.Name = "chk_Publisher";
            this.chk_Publisher.Properties.Caption = "Publisher";
            this.chk_Publisher.Size = new System.Drawing.Size(103, 20);
            this.chk_Publisher.TabIndex = 2;
            // 
            // chk_Authors
            // 
            this.chk_Authors.EditValue = true;
            this.chk_Authors.Location = new System.Drawing.Point(5, 83);
            this.chk_Authors.Name = "chk_Authors";
            this.chk_Authors.Properties.Caption = "Authors";
            this.chk_Authors.Size = new System.Drawing.Size(103, 20);
            this.chk_Authors.TabIndex = 2;
            // 
            // chk_Categorys
            // 
            this.chk_Categorys.EditValue = true;
            this.chk_Categorys.Location = new System.Drawing.Point(5, 109);
            this.chk_Categorys.Name = "chk_Categorys";
            this.chk_Categorys.Properties.Caption = "Categorys";
            this.chk_Categorys.Size = new System.Drawing.Size(103, 20);
            this.chk_Categorys.TabIndex = 2;
            // 
            // chk_Cover
            // 
            this.chk_Cover.EditValue = true;
            this.chk_Cover.Location = new System.Drawing.Point(212, 31);
            this.chk_Cover.Name = "chk_Cover";
            this.chk_Cover.Properties.Caption = "Cover";
            this.chk_Cover.Size = new System.Drawing.Size(103, 20);
            this.chk_Cover.TabIndex = 2;
            // 
            // chk_Price
            // 
            this.chk_Price.EditValue = true;
            this.chk_Price.Location = new System.Drawing.Point(212, 57);
            this.chk_Price.Name = "chk_Price";
            this.chk_Price.Properties.Caption = "Price";
            this.chk_Price.Size = new System.Drawing.Size(103, 20);
            this.chk_Price.TabIndex = 2;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(191, 78);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(135, 23);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "Start";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // chk_Date
            // 
            this.chk_Date.EditValue = true;
            this.chk_Date.Location = new System.Drawing.Point(103, 31);
            this.chk_Date.Name = "chk_Date";
            this.chk_Date.Properties.Caption = "Publish date";
            this.chk_Date.Size = new System.Drawing.Size(103, 20);
            this.chk_Date.TabIndex = 2;
            // 
            // chk_Pages
            // 
            this.chk_Pages.EditValue = true;
            this.chk_Pages.Location = new System.Drawing.Point(103, 57);
            this.chk_Pages.Name = "chk_Pages";
            this.chk_Pages.Properties.Caption = "Pages count";
            this.chk_Pages.Size = new System.Drawing.Size(103, 20);
            this.chk_Pages.TabIndex = 2;
            // 
            // chk_Rating
            // 
            this.chk_Rating.EditValue = true;
            this.chk_Rating.Location = new System.Drawing.Point(103, 83);
            this.chk_Rating.Name = "chk_Rating";
            this.chk_Rating.Properties.Caption = "Rating";
            this.chk_Rating.Size = new System.Drawing.Size(103, 20);
            this.chk_Rating.TabIndex = 2;
            // 
            // chk_Subjrct
            // 
            this.chk_Subjrct.EditValue = true;
            this.chk_Subjrct.Location = new System.Drawing.Point(103, 109);
            this.chk_Subjrct.Name = "chk_Subjrct";
            this.chk_Subjrct.Properties.Caption = "Subject";
            this.chk_Subjrct.Size = new System.Drawing.Size(103, 20);
            this.chk_Subjrct.TabIndex = 2;
            // 
            // chk_Discreption
            // 
            this.chk_Discreption.EditValue = true;
            this.chk_Discreption.Location = new System.Drawing.Point(212, 83);
            this.chk_Discreption.Name = "chk_Discreption";
            this.chk_Discreption.Properties.Caption = "Discreption";
            this.chk_Discreption.Size = new System.Drawing.Size(103, 20);
            this.chk_Discreption.TabIndex = 2;
            // 
            // chk_Isbn
            // 
            this.chk_Isbn.EditValue = true;
            this.chk_Isbn.Location = new System.Drawing.Point(212, 109);
            this.chk_Isbn.Name = "chk_Isbn";
            this.chk_Isbn.Properties.Caption = "ISBN";
            this.chk_Isbn.Size = new System.Drawing.Size(103, 20);
            this.chk_Isbn.TabIndex = 2;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(12, 470);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(316, 18);
            this.progressBarControl1.TabIndex = 4;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(191, 22);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(135, 22);
            this.spinEdit1.TabIndex = 5;
            // 
            // spinEdit2
            // 
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(191, 50);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit2.Size = new System.Drawing.Size(135, 22);
            this.spinEdit2.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(158, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 16);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "from";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(158, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(11, 16);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "to";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.chk_Discreption);
            this.groupControl1.Controls.Add(this.chk_Price);
            this.groupControl1.Controls.Add(this.chk_Subjrct);
            this.groupControl1.Controls.Add(this.chk_Cover);
            this.groupControl1.Controls.Add(this.chk_Rating);
            this.groupControl1.Controls.Add(this.chk_Categorys);
            this.groupControl1.Controls.Add(this.chk_Pages);
            this.groupControl1.Controls.Add(this.chk_Authors);
            this.groupControl1.Controls.Add(this.chk_Date);
            this.groupControl1.Controls.Add(this.chk_Publisher);
            this.groupControl1.Controls.Add(this.chk_Isbn);
            this.groupControl1.Controls.Add(this.chk_Title);
            this.groupControl1.Location = new System.Drawing.Point(11, 142);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(317, 140);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "groupControl1";
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(12, 12);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroup1.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "All"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "In range"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "In Range (by ID)")});
            this.radioGroup1.Properties.ItemsLayout = DevExpress.XtraEditors.RadioGroupItemsLayout.Flow;
            this.radioGroup1.Size = new System.Drawing.Size(121, 102);
            this.radioGroup1.TabIndex = 8;
            this.radioGroup1.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 120);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 16);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = ".....................";
            // 
            // frm_SyncWithGoogle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 500);
            this.Controls.Add(this.radioGroup1);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.spinEdit2);
            this.Controls.Add(this.spinEdit1);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.memoEdit1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_SyncWithGoogle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sync with google";
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Title.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Publisher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Authors.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Categorys.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Cover.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Price.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Pages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Rating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Subjrct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Discreption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_Isbn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.CheckEdit chk_Title;
        private DevExpress.XtraEditors.CheckEdit chk_Publisher;
        private DevExpress.XtraEditors.CheckEdit chk_Authors;
        private DevExpress.XtraEditors.CheckEdit chk_Categorys;
        private DevExpress.XtraEditors.CheckEdit chk_Cover;
        private DevExpress.XtraEditors.CheckEdit chk_Price;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit chk_Date;
        private DevExpress.XtraEditors.CheckEdit chk_Pages;
        private DevExpress.XtraEditors.CheckEdit chk_Rating;
        private DevExpress.XtraEditors.CheckEdit chk_Subjrct;
        private DevExpress.XtraEditors.CheckEdit chk_Discreption;
        private DevExpress.XtraEditors.CheckEdit chk_Isbn;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
    }
}