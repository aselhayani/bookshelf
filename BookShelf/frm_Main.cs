﻿using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BookShelf
{
    public partial class frm_Main : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {

        private static frm_Main _instance;
        public static frm_Main instance
        {
            get
            {
                if (_instance == null)
                    _instance = new frm_Main();
                return _instance;
            }
        }

        public frm_Main()
        {
            InitializeComponent();
        }
        protected override bool ExtendNavigationControlToFormTitle
        {
            get { return true ; }
        }
        public static void ShowMessage(string msg , byte state = 0)
        {
            instance.barStaticItem1.Caption = msg;
            switch (state)
            {
                case 0:
                    instance.barStaticItem1.ItemAppearance.Normal.BackColor = Color.Green;
                    break;
                case 1:
                    instance.barStaticItem1.ItemAppearance.Normal.BackColor = Color.Orange ;
                    break;
                case 2:
                    instance.barStaticItem1.ItemAppearance.Normal.BackColor = Color.Red;
                    break; 
            }
            instance.barStaticItem1.Visibility = BarItemVisibility.Always; 
        }

        public static System.Drawing.Image GetImageFromByte(Byte[] ByteArray)
        {
            System.Drawing.Image img;
            try
            {
                Byte[] imgbyte = ByteArray;
                MemoryStream strm = new MemoryStream(imgbyte, false);
                img = System.Drawing.Image.FromStream(strm);
            }
            catch { img = null; }
            return img;
        }
        public static Byte[] GetByteFromImage(System.Drawing.Image image)
        {
            MemoryStream strm = new MemoryStream();
            try
            {

                image.Save(strm, System.Drawing.Imaging.ImageFormat.Jpeg);
                return strm.ToArray();
            }
            catch
            {
                return strm.ToArray();
            }
        }
    public     void ShowControl(Control control)
        {
            if (!container.Controls.Contains(control))
            {
                container.Controls.Add(control);
                control.Dock = DockStyle.Fill;
            }
            control.BringToFront();

        }
        private void accordionControlElement5_Click(object sender, EventArgs e)
        {
            ShowControl(uc_Book.instance); 
        }

        private void accordionControlElement6_Click(object sender, EventArgs e)
        { 
            ShowControl(uc_BooksList.instance);

        }

        private void accordionControlElement4_Click(object sender, EventArgs e)
        {
            ShowControl(uc_Member.instance);

        }

        private void accordionControlElement19_Click(object sender, EventArgs e)
        {
            ShowControl(uc_AddListOfBooks .instance);

        }

        private void accordionControlElement20_Click(object sender, EventArgs e)
        {
            new frm_SyncWithGoogle().ShowDialog();
        }
    }
}
