﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq.Expressions;
using System.Diagnostics;
using DevExpress.XtraEditors.Repository;

namespace BookShelf
{
    public partial class uc_BookAuthors : DevExpress.XtraEditors.XtraForm
    {
        static BindingList<BookAuthor> _bookAuthors;
        static BindingList<BookCategory> _bookCategorys;

        RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
        bool _ShowCategory;

        public uc_BookAuthors(bool ShowCategory)
        {
            InitializeComponent();
            _ShowCategory = ShowCategory;
         
            BookShelfDBDataContext db = new BookShelfDBDataContext();


            if (_ShowCategory)
            {
                repo.DataSource = db.Categories.ToList();
                gridControl1.DataSource = _bookCategorys; 
                
            }
            else
            {
                gridControl1.DataSource = _bookAuthors; 

                repo.DataSource = db.Authors.ToList();
            }
            gridView1.Columns[0].Visible  =
            gridView1.Columns[1].Visible = false;
            repo.ValueMember = "ID";
            repo.DisplayMember = "Name";
            gridControl1.RepositoryItems.Add(repo);
            gridView1.Columns[2].ColumnEdit = repo;
            repo.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.True;
            repo.ProcessNewValue += Repo_ProcessNewValue;
            repo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            repo.NullText = "";

       }

        private void Repo_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        { 
            if(e.DisplayValue != null || e.DisplayValue.ToString().Trim() != "")
            {
                BookShelfDBDataContext db = new BookShelfDBDataContext();
                if (!_ShowCategory)
                {
                    var author = new Author()
                    {
                        Name = e.DisplayValue.ToString().Trim()
                    };
                    db.Authors.InsertOnSubmit(author);
                    db.SubmitChanges();
                    ((List<Author>)repo.DataSource).Add(author);
                }
                else
                {
                    var categ = new Category ()
                    {
                        Name = e.DisplayValue.ToString().Trim()
                    };
                    db.Categories.InsertOnSubmit(categ);
                    db.SubmitChanges();
                    ((List<Category>)repo.DataSource).Add(categ);
                }
                    
 
                e.Handled = true;
            }
        }

        public static BindingList<BookAuthor> ViewAuthors(BindingList<BookAuthor> bookAuthors)
        {
            _bookAuthors = bookAuthors; 
            uc_BookAuthors frm = new uc_BookAuthors(false);
            frm.ShowDialog();
            return _bookAuthors;
        }
        public static BindingList<BookCategory> ViewCategory(BindingList<BookCategory > bookCategorys)
        {
            _bookCategorys = bookCategorys;  
            uc_BookAuthors frm = new uc_BookAuthors(true);
            frm.ShowDialog();
            return _bookCategorys;
        }

      
    }
}
