﻿namespace BookShelf
{
    partial class uc_Book
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.spn_BPrice = new DevExpress.XtraEditors.SpinEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.txt_ISBN = new DevExpress.XtraEditors.ButtonEdit();
            this.ratingControl1 = new DevExpress.XtraEditors.RatingControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.txt_ID = new DevExpress.XtraEditors.TextEdit();
            this.txt_Titel = new DevExpress.XtraEditors.TextEdit();
            this.txt_Subject = new DevExpress.XtraEditors.TextEdit();
            this.txt_Discreption = new DevExpress.XtraEditors.MemoEdit();
            this.spn_Price = new DevExpress.XtraEditors.SpinEdit();
            this.spn_PagesCount = new DevExpress.XtraEditors.SpinEdit();
            this.textEdit14 = new DevExpress.XtraEditors.ButtonEdit();
            this.textEdit13 = new DevExpress.XtraEditors.ButtonEdit();
            this.txt_PublishDate = new DevExpress.XtraEditors.DateEdit();
            this.txt_Collection = new DevExpress.XtraEditors.LookUpEdit();
            this.txt_Langage = new DevExpress.XtraEditors.LookUpEdit();
            this.txt_Country = new DevExpress.XtraEditors.LookUpEdit();
            this.txt_Publisher = new DevExpress.XtraEditors.LookUpEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spn_BPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ISBN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratingControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Titel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Subject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Discreption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Price.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_PagesCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_PublishDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_PublishDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Collection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Langage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Country.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Publisher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.spn_BPrice);
            this.layoutControl1.Controls.Add(this.txt_ISBN);
            this.layoutControl1.Controls.Add(this.ratingControl1);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Controls.Add(this.txt_ID);
            this.layoutControl1.Controls.Add(this.txt_Titel);
            this.layoutControl1.Controls.Add(this.txt_Subject);
            this.layoutControl1.Controls.Add(this.txt_Discreption);
            this.layoutControl1.Controls.Add(this.spn_Price);
            this.layoutControl1.Controls.Add(this.spn_PagesCount);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.txt_PublishDate);
            this.layoutControl1.Controls.Add(this.txt_Collection);
            this.layoutControl1.Controls.Add(this.txt_Langage);
            this.layoutControl1.Controls.Add(this.txt_Country);
            this.layoutControl1.Controls.Add(this.txt_Publisher);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(875, 602);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // spn_BPrice
            // 
            this.spn_BPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_BPrice.Location = new System.Drawing.Point(351, 280);
            this.spn_BPrice.MenuManager = this.barManager1;
            this.spn_BPrice.Name = "spn_BPrice";
            this.spn_BPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_BPrice.Size = new System.Drawing.Size(184, 22);
            this.spn_BPrice.StyleController = this.layoutControl1;
            this.spn_BPrice.TabIndex = 8;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 5;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem3, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem2, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Save";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.ImageOptions.SvgImage = global::BookShelf.Properties.Resources.save;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "New";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.ImageOptions.SvgImage = global::BookShelf.Properties.Resources.actions_addcircled;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Delete";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.SvgImage = global::BookShelf.Properties.Resources.actions_deletecircled;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem5.Caption = "Prev";
            this.barButtonItem5.Id = 4;
            this.barButtonItem5.ImageOptions.SvgImage = global::BookShelf.Properties.Resources.prev;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem4.Caption = "Next";
            this.barButtonItem4.Id = 3;
            this.barButtonItem4.ImageOptions.SvgImage = global::BookShelf.Properties.Resources.next;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(875, 28);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 630);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(875, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 28);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 602);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(875, 28);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 602);
            // 
            // txt_ISBN
            // 
            this.txt_ISBN.Location = new System.Drawing.Point(351, 49);
            this.txt_ISBN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_ISBN.MenuManager = this.barManager1;
            this.txt_ISBN.Name = "txt_ISBN";
            this.txt_ISBN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search)});
            this.txt_ISBN.Size = new System.Drawing.Size(184, 22);
            this.txt_ISBN.StyleController = this.layoutControl1;
            this.txt_ISBN.TabIndex = 7;
            this.txt_ISBN.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txt_ISBN_ButtonClick);
            // 
            // ratingControl1
            // 
            this.ratingControl1.Location = new System.Drawing.Point(563, 556);
            this.ratingControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ratingControl1.MenuManager = this.barManager1;
            this.ratingControl1.Name = "ratingControl1";
            this.ratingControl1.Properties.AutoHeight = false;
            this.ratingControl1.Properties.AutoSize = false;
            this.ratingControl1.Rating = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ratingControl1.Size = new System.Drawing.Size(288, 22);
            this.ratingControl1.StyleController = this.layoutControl1;
            this.ratingControl1.TabIndex = 6;
            this.ratingControl1.Text = "ratingControl1";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(563, 49);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(288, 503);
            this.pictureEdit1.StyleController = this.layoutControl1;
            this.pictureEdit1.TabIndex = 5;
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(100, 49);
            this.txt_ID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_ID.MenuManager = this.barManager1;
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Properties.ReadOnly = true;
            this.txt_ID.Size = new System.Drawing.Size(171, 22);
            this.txt_ID.StyleController = this.layoutControl1;
            this.txt_ID.TabIndex = 4;
            // 
            // txt_Titel
            // 
            this.txt_Titel.Location = new System.Drawing.Point(100, 75);
            this.txt_Titel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Titel.Name = "txt_Titel";
            this.txt_Titel.Size = new System.Drawing.Size(435, 22);
            this.txt_Titel.StyleController = this.layoutControl1;
            this.txt_Titel.TabIndex = 4;
            // 
            // txt_Subject
            // 
            this.txt_Subject.Location = new System.Drawing.Point(100, 381);
            this.txt_Subject.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Subject.Name = "txt_Subject";
            this.txt_Subject.Size = new System.Drawing.Size(435, 22);
            this.txt_Subject.StyleController = this.layoutControl1;
            this.txt_Subject.TabIndex = 4;
            // 
            // txt_Discreption
            // 
            this.txt_Discreption.Location = new System.Drawing.Point(100, 407);
            this.txt_Discreption.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Discreption.Name = "txt_Discreption";
            this.txt_Discreption.Size = new System.Drawing.Size(435, 171);
            this.txt_Discreption.StyleController = this.layoutControl1;
            this.txt_Discreption.TabIndex = 4;
            // 
            // spn_Price
            // 
            this.spn_Price.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Price.Location = new System.Drawing.Point(351, 254);
            this.spn_Price.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Price.Name = "spn_Price";
            this.spn_Price.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Price.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spn_Price.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spn_Price.Size = new System.Drawing.Size(184, 22);
            this.spn_Price.StyleController = this.layoutControl1;
            this.spn_Price.TabIndex = 4;
            // 
            // spn_PagesCount
            // 
            this.spn_PagesCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_PagesCount.Location = new System.Drawing.Point(100, 254);
            this.spn_PagesCount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_PagesCount.Name = "spn_PagesCount";
            this.spn_PagesCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_PagesCount.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spn_PagesCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spn_PagesCount.Size = new System.Drawing.Size(171, 22);
            this.spn_PagesCount.StyleController = this.layoutControl1;
            this.spn_PagesCount.TabIndex = 4;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(100, 179);
            this.textEdit14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.textEdit14.Properties.ReadOnly = true;
            this.textEdit14.Size = new System.Drawing.Size(435, 22);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 4;
            this.textEdit14.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.textEdit14_ButtonClick);
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(100, 153);
            this.textEdit13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.textEdit13.Properties.ReadOnly = true;
            this.textEdit13.Size = new System.Drawing.Size(435, 22);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 4;
            this.textEdit13.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.textEdit13_ButtonClick);
            // 
            // txt_PublishDate
            // 
            this.txt_PublishDate.EditValue = null;
            this.txt_PublishDate.Location = new System.Drawing.Point(351, 101);
            this.txt_PublishDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_PublishDate.Name = "txt_PublishDate";
            this.txt_PublishDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_PublishDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_PublishDate.Properties.DisplayFormat.FormatString = "";
            this.txt_PublishDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txt_PublishDate.Properties.EditFormat.FormatString = "";
            this.txt_PublishDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txt_PublishDate.Properties.Mask.EditMask = "";
            this.txt_PublishDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txt_PublishDate.Size = new System.Drawing.Size(184, 22);
            this.txt_PublishDate.StyleController = this.layoutControl1;
            this.txt_PublishDate.TabIndex = 4;
            // 
            // txt_Collection
            // 
            this.txt_Collection.Location = new System.Drawing.Point(100, 127);
            this.txt_Collection.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Collection.Name = "txt_Collection";
            this.txt_Collection.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.True;
            this.txt_Collection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_Collection.Properties.NullText = "";
            this.txt_Collection.Properties.PopupSizeable = false;
            this.txt_Collection.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.txt_Collection.Size = new System.Drawing.Size(435, 22);
            this.txt_Collection.StyleController = this.layoutControl1;
            this.txt_Collection.TabIndex = 4;
            // 
            // txt_Langage
            // 
            this.txt_Langage.Location = new System.Drawing.Point(100, 280);
            this.txt_Langage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Langage.Name = "txt_Langage";
            this.txt_Langage.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.True;
            this.txt_Langage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_Langage.Properties.NullText = "";
            this.txt_Langage.Properties.PopupSizeable = false;
            this.txt_Langage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.txt_Langage.Size = new System.Drawing.Size(171, 22);
            this.txt_Langage.StyleController = this.layoutControl1;
            this.txt_Langage.TabIndex = 4;
            // 
            // txt_Country
            // 
            this.txt_Country.Location = new System.Drawing.Point(100, 306);
            this.txt_Country.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Country.Name = "txt_Country";
            this.txt_Country.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.True;
            this.txt_Country.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_Country.Properties.NullText = "";
            this.txt_Country.Properties.PopupSizeable = false;
            this.txt_Country.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.txt_Country.Size = new System.Drawing.Size(171, 22);
            this.txt_Country.StyleController = this.layoutControl1;
            this.txt_Country.TabIndex = 4;
            // 
            // txt_Publisher
            // 
            this.txt_Publisher.Location = new System.Drawing.Point(100, 101);
            this.txt_Publisher.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_Publisher.Name = "txt_Publisher";
            this.txt_Publisher.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.True;
            this.txt_Publisher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt_Publisher.Properties.NullText = "";
            this.txt_Publisher.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.txt_Publisher.Size = new System.Drawing.Size(171, 22);
            this.txt_Publisher.StyleController = this.layoutControl1;
            this.txt_Publisher.TabIndex = 4;
            this.txt_Publisher.ProcessNewValue += new DevExpress.XtraEditors.Controls.ProcessNewValueEventHandler(this.txt_Publisher_ProcessNewValue);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup1,
            this.layoutControlGroup4});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(875, 602);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem17});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(539, 205);
            this.layoutControlGroup2.Text = "Main info";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt_ID;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(251, 26);
            this.layoutControlItem1.Text = "ID";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt_Titel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(515, 26);
            this.layoutControlItem2.Text = "Titel";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt_Publisher;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(251, 26);
            this.layoutControlItem5.Text = "Publisher";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txt_Collection;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(515, 26);
            this.layoutControlItem6.Text = "Collection";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt_PublishDate;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem4.Location = new System.Drawing.Point(251, 52);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(264, 26);
            this.layoutControlItem4.Text = "PublishDate";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textEdit13;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(515, 26);
            this.layoutControlItem13.Text = "Auther";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.textEdit14;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 130);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(515, 26);
            this.layoutControlItem14.Text = "Catagorys";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt_ISBN;
            this.layoutControlItem17.Location = new System.Drawing.Point(251, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(264, 26);
            this.layoutControlItem17.Text = "ISBN";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.layoutControlGroup3.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem10,
            this.layoutControlItem12});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 205);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(539, 127);
            this.layoutControlGroup3.Text = "Other info";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.spn_PagesCount;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(251, 26);
            this.layoutControlItem11.Text = "PagesCount";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.spn_Price;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem9.Location = new System.Drawing.Point(251, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(264, 26);
            this.layoutControlItem9.Text = "Price";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.spn_BPrice;
            this.layoutControlItem3.Location = new System.Drawing.Point(251, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(264, 52);
            this.layoutControlItem3.Text = "Borrow price";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt_Langage;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(251, 26);
            this.layoutControlItem10.Text = "Language";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt_Country;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(251, 26);
            this.layoutControlItem12.Text = "Country";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 332);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(539, 250);
            this.layoutControlGroup1.Text = "Content discreption";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt_Subject;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(515, 26);
            this.layoutControlItem7.Text = "Subject";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt_Discreption;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(515, 175);
            this.layoutControlItem8.Text = "Discreption";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(73, 16);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.layoutControlItem16});
            this.layoutControlGroup4.Location = new System.Drawing.Point(539, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(316, 582);
            this.layoutControlGroup4.Text = "Cover";
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.pictureEdit1;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(292, 507);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.ratingControl1;
            this.layoutControlItem16.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 507);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(292, 26);
            this.layoutControlItem16.Text = "Rate";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // uc_Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "uc_Book";
            this.Size = new System.Drawing.Size(875, 630);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spn_BPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ISBN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratingControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Titel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Subject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Discreption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Price.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_PagesCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_PublishDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_PublishDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Collection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Langage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Country.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Publisher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraEditors.TextEdit txt_ID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txt_Titel;
        private DevExpress.XtraEditors.TextEdit txt_Subject;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.RatingControl ratingControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraEditors.ButtonEdit txt_ISBN;
        private DevExpress.XtraEditors.MemoEdit txt_Discreption;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.SpinEdit spn_Price;
        private DevExpress.XtraEditors.SpinEdit spn_PagesCount;
        private DevExpress.XtraEditors.ButtonEdit textEdit14;
        private DevExpress.XtraEditors.ButtonEdit textEdit13;
        private DevExpress.XtraEditors.DateEdit txt_PublishDate;
        private DevExpress.XtraEditors.LookUpEdit txt_Collection;
        private DevExpress.XtraEditors.LookUpEdit txt_Langage;
        private DevExpress.XtraEditors.LookUpEdit txt_Country;
        private DevExpress.XtraEditors.SpinEdit spn_BPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit txt_Publisher;
    }
}

