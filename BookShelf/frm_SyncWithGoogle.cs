﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;

namespace BookShelf
{
    public partial class frm_SyncWithGoogle : DevExpress.XtraEditors.XtraForm
    {
        public frm_SyncWithGoogle()
        {
            InitializeComponent();
            memoEdit1.EditValueChanged += memoEdit1_EditValueChanged;
        }
        void memoEdit1_EditValueChanged(object sender, EventArgs e)
        {
            BeginInvoke(new MethodInvoker(delegate
            {
                SetSelection();
            }));
        }
        private void SetSelection()
        {
            memoEdit1.SelectionStart = memoEdit1.Text.Length;
            memoEdit1.ScrollToCaret();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            simpleButton1.Enabled = false;
            Task.Run(() => Run());
        }
        void Run()
        {
            BookShelfDBDataContext db = new BookShelfDBDataContext();
            List<BookAuthor> authors = new List<BookAuthor>();
            List<BookCategory> categorys = new List<BookCategory>();
            Invoke((MethodInvoker)(() => memoEdit1.Text += "Started! \n" + Environment.NewLine));
            var books = db.Books.Select(x=>x.ID );
            int start;
            int end;
            int.TryParse(spinEdit1.EditValue.ToString(), out start);
            int.TryParse(spinEdit2.EditValue.ToString(), out end);
            if (radioGroup1.SelectedIndex == 1) 
                books = db.Books.Skip(start).Take(end - start).Select(x => x.ID);
            else if (radioGroup1.SelectedIndex == 2)
                books = books.Where(x=> x >= start && x<= end );


            int pos = 0;
            Invoke((MethodInvoker)(() => progressBarControl1.Properties.Maximum = books.Count()));

            foreach (var id in books.ToList())
            {
                Book book = new Book();
                Book item = db.Books.Where(x => x.ID == id).Single();
                book.Titel = item.Titel;
                book.ISBN = item.ISBN;
                Invoke((MethodInvoker)(() => memoEdit1.Text += "Getting "+book.ISBN + Environment.NewLine));
              //  Thread.Sleep(500);
                int resultCode = uc_Book.DownloadBookData(book, out authors, out categorys);
                if (resultCode == 0)
                {
                    Invoke((MethodInvoker)(() => memoEdit1.Text += "'" + book.Titel + "' Sync faild .Could not connect " + Environment.NewLine)); 
                }
                else if (resultCode == 1)
                {
                    Invoke((MethodInvoker)(() => memoEdit1.Text += "'" + book.Titel + "' Sync faild . Book wasn't found " + Environment.NewLine)); 
                }
                else if (resultCode == 2)
                {

                    if (chk_Cover.Checked)
                        item.Cover = book.Cover;
                    if (chk_Date.Checked && book.PublishDate.HasValue && book.PublishDate.Value.Year < 2070 && book.PublishDate.Value.Year > 1750)
                        item.PublishDate = book.PublishDate;
                    if (chk_Discreption.Checked)
                        item.Discreption = book.Discreption;
                    if (chk_Isbn.Checked)
                        item.ISBN = book.ISBN;
                    if (chk_Pages.Checked)
                        item.PagesCount = book.PagesCount;
                    if (chk_Price.Checked)
                        item.Price = book.Price;
                    if (chk_Publisher.Checked)
                        item.Publisher = book.Publisher;
                    if (chk_Rating.Checked)
                        item.Rate = book.Rate;
                    if (chk_Subjrct.Checked)
                        item.Subject = book.Subject;
                    if (chk_Title.Checked)
                        item.Titel = book.Titel;
                    if (chk_Authors.Checked)
                    {
                        db.BookAuthors.DeleteAllOnSubmit(db.BookAuthors.Where(x => x.BookID == item.ID));
                        db.SubmitChanges();
                        db.BookAuthors.InsertAllOnSubmit(authors);
                    }
                    if (chk_Categorys.Checked)
                    {
                        db.BookCategories.DeleteAllOnSubmit(db.BookCategories.Where(x => x.BookID == item.ID));
                        db.BookCategories.InsertAllOnSubmit(categorys);
                        db.SubmitChanges();
                    }
                    db.SubmitChanges();

                    Invoke((MethodInvoker)(() => memoEdit1.Text += "'" + book.Titel + "' Synced successfully" + Environment.NewLine));
                }
                pos++;
                Invoke((MethodInvoker)(() => progressBarControl1.Position = pos));

            }
            Invoke((MethodInvoker)(() => memoEdit1.Text +="Done! \n" ));
            Invoke((MethodInvoker)(() => simpleButton1.Enabled = true));
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(radioGroup1.SelectedIndex== 0 )
                labelControl2.Visible = labelControl1.Visible = spinEdit1.Visible = spinEdit2.Visible = false;
            else
                labelControl2.Visible = labelControl1.Visible = spinEdit1.Visible = spinEdit2.Visible = true ;


        }
    }
}